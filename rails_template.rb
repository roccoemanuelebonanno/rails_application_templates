# frozen_string_literal: true

# Rspec...
puts '🚀 Starting off with RSpec!'
run "rails app:template LOCATION='https://gitlab.com/roccoemanuelebonanno/rails_application_templates/-/raw/main/rspec.rb?inline=true'"

# Rubocop
puts '🚀 Moving on to install Rubocop!'
run "rails app:template LOCATION='https://gitlab.com/roccoemanuelebonanno/rails_application_templates/-/raw/main/rubocop.rb?inline=true'"

# Shoulda Matchers
puts '🚀 Moving on to install ShouldaMatchers!'
run "rails app:template LOCATION='https://gitlab.com/roccoemanuelebonanno/rails_application_templates/-/raw/main/shoulda_matchers.rb?inline=true'"

# Factory Bot
puts '🚀 Moving on to install FactoryBot!'
run "rails app:template LOCATION='https://gitlab.com/roccoemanuelebonanno/rails_application_templates/-/raw/main/factory_bot.rb?inline=true'"

# Fuubar
puts '🚀 Moving on to install Fuubar!'
run "rails app:template LOCATION='https://gitlab.com/roccoemanuelebonanno/rails_application_templates/-/raw/main/fuubar.rb?inline=true'"

# Pry
puts '🚀 Moving on to install Pry!'
run "rails app:template LOCATION='https://gitlab.com/roccoemanuelebonanno/rails_application_templates/-/raw/main/pry.rb?inline=true'"

# Simple Cov
puts '🚀 Moving on to install SimpleCov!'
run "rails app:template LOCATION='https://gitlab.com/roccoemanuelebonanno/rails_application_templates/-/raw/main/simplecov.rb?inline=true'"

# VCR
puts '🚀 Moving on to install VCR!'
run "rails app:template LOCATION='https://gitlab.com/roccoemanuelebonanno/rails_application_templates/-/raw/main/vcr.rb?inline=true'"

# Config
puts '🚀 Moving on to install Config!'
run "rails app:template LOCATION='https://gitlab.com/roccoemanuelebonanno/rails_application_templates/-/raw/main/config.rb?inline=true'"

# Dotenv
puts '🚀 Moving on to install Dotenv!'
run "rails app:template LOCATION='https://gitlab.com/roccoemanuelebonanno/rails_application_templates/-/raw/main/dotenv.rb?inline=true'"

prepend_to_file 'spec/spec_helper.rb', <<~TEMPLATE
  Dir[File.expand_path('support/**/*.rb', __dir__)].each { |f| require f }\n
TEMPLATE
