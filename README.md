# rails_application_templates

## Getting started
Application templates are simple Ruby files containing DSL for adding gems, initializers, etc. to your freshly created Rails project or an existing Rails project. cit [Official Rails Site](https://guides.rubyonrails.org/rails_application_templates.html)

### Why
when you need to create a new project, you definitely need an initial setup phase. This little template will help you configure most of popular and used gems:
- [FactoryBot Rails](https://github.com/thoughtbot/factory_bot_rails)
- [Fuubar](https://github.com/thekompanee/fuubar)
- [Pry](https://github.com/pry/pry)
- [Rspec](https://github.com/rspec/rspec-rails)
- [Rubocop](https://github.com/rubocop/rubocop)
- [Shoulda Matchers](https://github.com/thoughtbot/shoulda-matchers)
- [Simplecov](https://github.com/simplecov-ruby/simplecov)

## How to use
If you want, you can clone this repo, in local machine to apport some customization, and when finish, you may need to run

`$ rails new NAME_OF_YOUR_APP -m RAILS_APPLICATION_TEMPLATE_PATH/rails_template.rb`

Or more simple, you can pass directly the url:

`$ rails new NAME_OF_YOUR_APP -m https://gitlab.com/roccoemanuelebonanno/rails_application_templates/-/raw/main/rails_template.rb?inline=true`

## Thanks 
Pull requests and improvements are always welcome.