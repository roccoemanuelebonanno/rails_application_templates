# frozen_string_literal: true

gem_group :development, :test do
  gem 'vcr', require: false
end

after_bundle do
  file 'spec/support/vcr.rb', <<~CODE
    require "vcr"
    VCR.configure do |config|
      config.cassette_library_dir = Rails.root.join("spec/cassettes")
      config.hook_into :webmock
      config.configure_rspec_metadata!
      config.default_cassette_options = { record: :once }
      config.debug_logger = $stdout if ENV["VCR_DEBUG"]
    end

    RSpec.configure do |config|
      config.around(:each, :vcr) do |example|
        name = example.metadata[:full_description].split(/\s+/, 2).join("/").underscore.gsub(
          %r{[^\w/]+}, "_"
        )
        options = example.metadata.slice(:record, :match_requests_on).except(:example_group)
        VCR.use_cassette(name, options) { example.call }
      end
    end
  CODE
end
