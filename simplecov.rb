# frozen_string_literal: true

gem_group :development, :test do
  gem 'simplecov', require: false
end

after_bundle do
  file 'spec/support/simplecov.rb', <<~CODE
    require "simplecov"

    SimpleCov.start do
      enable_coverage :branch
      add_filter "/spec/"
    end
  CODE
end
