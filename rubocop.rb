# frozen_string_literal: true

gem_group :development, :test do
  gem 'rubocop', require: false
  gem 'rubocop-factory_bot', require: false
  gem 'rubocop-performance', require: false
  gem 'rubocop-rails', require: false
  gem 'rubocop-rspec', require: false
end

after_bundle do
  run 'rubocop --auto-gen-config'
  run 'rubocop --format offenses'
end
