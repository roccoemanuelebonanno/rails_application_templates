# frozen_string_literal: true

gem_group :development, :test do
  gem 'config'
end

after_bundle do
  rails_command 'g config:install'
end
