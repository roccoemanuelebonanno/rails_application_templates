# frozen_string_literal: true

gem_group :development, :test do
  gem 'dotenv-rails', require: false
end

after_bundle do
  inject_into_file 'config/application.rb', after: "Bundler.require(*Rails.groups)\n" do
    <<~RUBY
      Dotenv::Railtie.load unless Rails.env.production?
    RUBY
  end
end
