# frozen_string_literal: true

gem_group :development, :test do
  gem 'factory_bot_rails'
end

after_bundle do
  file 'spec/support/factory_bot.rb', <<~CODE
    RSpec.configure do |config|
      config.include FactoryBot::Syntax::Methods
    end
  CODE
end
