# frozen_string_literal: true

gem_group :development, :test do
  gem 'pry-rails', require: false
  gem 'pry', require: false
end
