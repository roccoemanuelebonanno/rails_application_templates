# frozen_string_literal: true

gem_group :development, :test do
  gem 'shoulda-matchers'
end

after_bundle do
  file 'spec/support/shoulda_matchers.rb', <<~CODE
    Shoulda::Matchers.configure do |config|
      config.integrate do |with|
        with.test_framework :rspec
        with.library :rails
      end
    end
  CODE
end
