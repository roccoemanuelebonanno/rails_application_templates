# frozen_string_literal: true

run 'spring stop'
gem_group :development, :test do
  gem 'rspec-rails'
end

after_bundle do
  run 'bundle install'
  rails_command 'generate rspec:install'
  run 'rm -rf test'
end
