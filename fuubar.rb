# frozen_string_literal: true

gem_group :development, :test do
  gem 'fuubar', require: false
end

after_bundle do
  file 'spec/support/fuubar.rb', <<~CODE
    RSpec.configure do |config|
      config.add_formatter 'Fuubar'
      config.fuubar_auto_refresh = true
    end
  CODE
end
